export class Ingredient {
  constructor(
    public imageUrl: string,
    public name: string,
    public cost: number,
  ){}
}
