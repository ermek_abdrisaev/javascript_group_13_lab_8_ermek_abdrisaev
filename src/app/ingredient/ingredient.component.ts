import {Component, Input} from '@angular/core';
import {Ingredient} from "../Shared/ingredient.modal";

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent  {

  @Input() ingredients!: Ingredient[];

}
