import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BurgerCalculatorComponent } from './burger-calculator/burger-calculator.component';
import { IngredientsComponent } from './burger-calculator/ingredients/ingredients.component';
import { PriceComponent } from './burger-calculator/price/price.component';
import { FormsModule } from "@angular/forms";
import { IngredientComponent } from './ingredient/ingredient.component';

@NgModule({
  declarations: [
    AppComponent,
    BurgerCalculatorComponent,
    IngredientsComponent,
    PriceComponent,
    IngredientComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
