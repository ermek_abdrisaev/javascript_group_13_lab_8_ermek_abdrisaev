import {Component, Input} from '@angular/core';
import {Ingredient} from "../Shared/ingredient.modal";
import {IngredientsComponent} from "./ingredients/ingredients.component";

@Component({
  selector: 'app-burger-calculator',
  templateUrl: './burger-calculator.component.html',
  styleUrls: ['./burger-calculator.component.css']
})

export class BurgerCalculatorComponent {
  @Input() ingredients: Ingredient[] = [];



  getTotal(){
    return this.ingredients.reduce((acc, Ingredient) => {
      return acc + Ingredient.cost;
    }, 0);
  }

}
