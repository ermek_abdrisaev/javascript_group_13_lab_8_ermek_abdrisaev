import {Component, Input} from '@angular/core';
import {Ingredient} from "../../Shared/ingredient.modal";

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})
export class PriceComponent{

  @Input() Ingredients: Ingredient[] = [];

  getTotalPrice(){
    return this.Ingredients.reduce((acc, Ingredient) =>{
      return acc + Ingredient.cost;
    }, 0);
  }
}
