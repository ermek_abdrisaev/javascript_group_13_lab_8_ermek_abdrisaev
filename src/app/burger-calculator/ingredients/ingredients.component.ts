import {Component, Input} from '@angular/core';
import {Ingredient} from "../../Shared/ingredient.modal";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})

export class IngredientsComponent{
  totalSum: number = 0;
  quantaty: number = 0;

  onDeleteClick(){
    this.quantaty = 0;
    console.log("delete");
  }

  onImageClick(){
    this.quantaty ++;
    console.log(this.quantaty)
  }

}
